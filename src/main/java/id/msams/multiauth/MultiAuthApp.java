package id.msams.multiauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiAuthApp {

  public static void main(String[] args) {
    SpringApplication.run(MultiAuthApp.class, args);
  }

}
