package id.msams.multiauth.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.resource.web.access.BearerTokenAccessDeniedHandler;

import id.msams.multiauth.configuration.property.SecurityProp;
import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder passwordEncoder;
  private final SecurityProp securityProperties;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // @formatter:off

    // Generic HTTP Configuration
    http
      .authorizeRequests()
        .antMatchers(HttpMethod.POST, "/login/jwt/token").permitAll()
        .anyRequest().authenticated()
        .and()
      .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
        .sessionFixation().changeSessionId()
        .sessionConcurrency(session ->
          session
            .maximumSessions(1)
            .maxSessionsPreventsLogin(false)
        )
        .and()
      .csrf()
        .disable()
    ;

    // Configuration for HTTP Basic and JWT Auth
    http
      .httpBasic(Customizer.withDefaults())
      .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
      .exceptionHandling(exceptions ->
        exceptions
          .accessDeniedHandler(new BearerTokenAccessDeniedHandler())
      )
    ;

    // Configuration for OAuth 2.0 and OpenID Connect 1.0 Auth
    http
      .oauth2Login(Customizer.withDefaults())
    ;

    // Configuration for SAML 2.0 Auth
    http
      .saml2Login(Customizer.withDefaults())
      .saml2Logout(Customizer.withDefaults())
    ;

    // @formatter:on
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    // @formatter:off
    auth
      .inMemoryAuthentication()
        .passwordEncoder(passwordEncoder)
        .withUser(securityProperties.getDefaultLogin())
          .password(passwordEncoder.encode(securityProperties.getDefaultPassword()))
          .roles("SYSTEM_ADMIN")
    ;
    // @formatter:on
  }

}