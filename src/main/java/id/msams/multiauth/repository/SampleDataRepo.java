package id.msams.multiauth.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import id.msams.multiauth.model.SampleData;

@RepositoryRestResource(path = "sample-datas")
public interface SampleDataRepo extends JpaRepository<SampleData, String> {

  @Override
  @RestResource
  Page<SampleData> findAll(Pageable pageable);

}
